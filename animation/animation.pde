import codeanticode.syphon.*;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;
import gab.opencv.*;
import processing.video.*;
import java.awt.*;

Capture camera;

enum AnimationType {
  RUN, 
  WALK, 
  FIRE, 
  MAMMOTH,
  TIGER,
  BIRD,
  WOLF,
  ANTELOPE,
  TURTLE
};

final boolean DEBUG = true;
final int DELAY = 50;
final int TORCH_RADIUS = 150;
final int TORCH_BLUR_SIZE = 80;

ConcurrentHashMap<AnimationType, PImage[]> Animations = new ConcurrentHashMap<AnimationType, PImage[]>();

void loadAnimation(AnimationType t, String path, Integer n) {
  PImage[] images = new PImage[n];

  for (int i = 0; i < n; i++) {
    // Use nf() to number format 'i' into four digits
    String filename = path + i + ".png";
    images[i] = loadImage(filename);
  }
  Animations.put(t, images);
}

AnimEntity character, tiger, mammoth, antelope, wolf, turtle;
ArrayList<AnimEntity> drawable = new ArrayList<AnimEntity>();
AnimEntity[] birds = {};
AnimEntity[] crowd = {};
PVector[] crowdPath = {};

SyphonServer server;
PGraphics canvas, torchlight, pass1, pass2;
PShader blur;

color bg = color(0, 255);
int last;  
color torchColor = #FFCB6A;
OpenCV opencv;

void setup() {
  opencv = new OpenCV(this, 640, 480);

  size(1920, 480, P3D);
  frameRate(30);
  background(bg);
  print("Loading...");
  loadAnimation(AnimationType.RUN, "animations/run/run00", 4);
  loadAnimation(AnimationType.WALK, "animations/walk/walk00", 5);
  loadAnimation(AnimationType.FIRE, "animations/fire/fire00", 4);
  loadAnimation(AnimationType.MAMMOTH, "animations/mammoth/mammoth00", 4);
  loadAnimation(AnimationType.TIGER, "animations/tiger/tiger00", 4);
  loadAnimation(AnimationType.BIRD, "animations/birds/bird00", 4);
  loadAnimation(AnimationType.ANTELOPE, "animations/antelope/antelope00", 4);
  loadAnimation(AnimationType.WOLF, "animations/wolf/wolf00", 5);
  loadAnimation(AnimationType.TURTLE, "animations/turtle/turtle00", 4);
  try {
    Thread.sleep(4);
  } 
  catch(Exception e) {
  }
  println("DONE LOADING");
  character = new AnimEntity(AnimationType.RUN, 20, 20, 2, 0.2);
  tiger = new AnimEntity(AnimationType.TIGER, 80, 80, 2, 0.2);
  mammoth = new AnimEntity(AnimationType.MAMMOTH, width / 2 + 40, 40, 2, 0.5);
  antelope = new AnimEntity(AnimationType.ANTELOPE, 20, 40, 2, 0.2);
  wolf = new AnimEntity(AnimationType.WOLF, 50, 50, 2, 0.2);
  turtle = new AnimEntity(AnimationType.TURTLE, 0, height, 2, 0.1);
  drawable.add(turtle);
  drawable.add(wolf);
  drawable.add(new AnimEntity(AnimationType.FIRE, width / 2, height / 2, 2, 0.2));
  drawable.add(character);
  drawable.add(tiger);
  drawable.add(mammoth);
  drawable.add(antelope);
  
  for(int i = 0; i < 4; i++) {
    AnimEntity e = new AnimEntity(AnimationType.BIRD, 200, 200, 2, 0.2);
    drawable.add(e);
    e.frame += random(10) % e.imageCount;
    birds = (AnimEntity[]) append(birds, e);
  }
  
  final int NUM_IN_CROWD = 4;
  for(int i = 0; i < NUM_IN_CROWD; i++) {
    PVector g = new PVector(((width / 2) + cos(((2 * PI) / NUM_IN_CROWD) * i) * 100),   
      ((height / 2) + sin(((2 * PI) / NUM_IN_CROWD) * i) * 100));
    AnimEntity e = new AnimEntity(AnimationType.WALK, 
      (int) g.x, (int) g.y, 2, 0.2);  
    e.curPathI = i;
    drawable.add(e);
    crowd = (AnimEntity[]) append(crowd, e);
    crowdPath = (PVector[]) append(crowdPath, g);
  }

  ///

  torchlight = createGraphics(width, height);
  canvas = createGraphics(width, height);

  blur = loadShader("blur.glsl");
  blur.set("blurSize", TORCH_BLUR_SIZE);
  blur.set("sigma", 150.0f);

  pass1 = createGraphics(width, height, P3D);
  pass1.noSmooth();

  pass2 = createGraphics(width, height, P3D);
  pass2.noSmooth();
  
  camera = new Capture(this, 640, 480, "USB 2.0 Camera");
  camera.start();

  last = millis();

  server = new SyphonServer(this, "Processing Syphon");
}

PVector pos = new PVector(0, 0);
PImage cvImg;

PVector newp = new PVector(0, 0);
PVector torchWorld = new PVector(0, 0);
float torchScale = 1;
void draw() {
  clear();
  
  opencv.loadImage(camera);
  opencv.gray();
  opencv.brightness(-200);
  opencv.contrast(1.3);
  opencv.blur(11);
  opencv.threshold(30);
  //opencv.updateBackground();
  opencv.dilate();
  opencv.erode();
  
  if (DEBUG) {
    cvImg = opencv.getOutput();
  }

  ArrayList<Contour> contours;
    contours = opencv.findContours();
    for (Contour contour : contours) {
      if(contour.area() < 800)
       continue;
    stroke(0, 255, 0);
    contour.draw();
    
    Rectangle r = contour.getBoundingBox();
    noFill();
    rect(r.x, r.y, r.width, r.height); 
    newp = new PVector(r.x + r.width / 2, r.y + r.height / 2);
  }
  
  if (!(newp.y == 0)) {
    if(DEBUG) {
      ellipse(newp.x, newp.y, 10, 10);
    }
    pos = PVector.add(pos, PVector.sub(newp, pos).limit(50));
    torchWorld = pos.copy();
    torchScale = height / (pos.y + height);
    torchWorld.y = height/2;
    torchWorld.x = torchWorld.x * 3;
  }
  
  torchlight.beginDraw();
  torchlight.noStroke();
  torchlight.background(bg);
  torchlight.ellipseMode(CENTER);
  torchlight.ellipse(torchWorld.x, torchWorld.y, TORCH_RADIUS * 2 * torchScale + random(-60, 60), TORCH_RADIUS * 2 * torchScale + random(-60, 60));
  torchlight.endDraw();

  blur.set("horizontalPass", 1);
  pass1.beginDraw();
  pass1.shader(blur);
  pass1.image(torchlight, 0, 0);
  pass1.endDraw();

  blur.set("horizontalPass", 0);
  pass2.beginDraw();
  pass2.shader(blur);
  pass2.image(pass1, 0, 0);
  pass2.endDraw();
  
  final PVector[] mammothPath = {
    new PVector(width / 2 + 200, 200), 
    new PVector(width / 2 + 200, height - 200), 
    new PVector(width - 200, 200), 
    new PVector(width - 200, height - 200)
  };
  
  final PVector[] turtlePath = {new PVector(0, height - 80), new PVector(width, height - 80)};
  
  PVector mg = wanderGoal(mammoth, getPathGoal(mammoth, mammothPath));
  applyVelocity(mammoth, instantSeek(mammoth, mg, 5), mg, 1);
  PVector tg = getPathGoal(turtle, turtlePath);
  applyVelocity(turtle, instantSeek(turtle, tg, 2), tg, 1);

  final PVector[] antelopePath = {new PVector(100, height - 100), new PVector(width / 2 - 100, height - 100), new PVector(width / 4, 100)};
  PVector ag = wanderGoal(antelope, getPathGoal(antelope, antelopePath));
  applyVelocity(antelope, instantSeek(antelope, ag, 10), ag, 1);
  
  PVector wv = instantSeek(wolf, antelope.curPos, 5);
  applyVelocity(wolf, wv, antelope.curPos, 1); 
  
  updateBirds(birds);
  for(AnimEntity e : crowd) {
    PVector goal = getPathGoal(e, crowdPath);
    PVector v = instantSeek(e, goal, 10);
    applyVelocity(e, v, e.curPos, 1);
  }

  // Display the sprite at the position xpos, ypos
  canvas.beginDraw();
  if (frameCount % 7 == 0) {
    canvas.fill(torchColor);
    canvas.rect(0, 0, width, height);
    //canvas.background(100, 10);
    
    PVector v = instantSeek(character, torchWorld, 10);
    applyVelocity(character, v, torchWorld, 1);
    updateTiger(tiger, character.curPos, torchWorld);

    Collections.sort(drawable);
    for (AnimEntity e : drawable) {
      drawAnim(canvas, e);
    }
  }

  canvas.endDraw();
  canvas.mask(pass2);
  image(canvas, 0, 0);

  if (DEBUG) {
    tint(255, 255, 255, 100);
    image(camera, 0, 0);
    if (cvImg != null)
      image(cvImg, 800, 0);
    tint(255, 255, 255, 255);
  }
  server.sendImage(canvas);
}

void captureEvent(Capture c) {
  c.read();
}

class AnimEntity implements Comparable {
  PVector curPos;
  int curPathI = 0; // Not normally used, only for path following
  PVector curV = new PVector(0, 0);
  AnimationType animationType;
  int imageCount;
  int frame = 0;
  final float MAX_VELOCITY = 40.0;
  int dir = 1;
  float scale;
  int z;
  AnimEntity(AnimationType a, int x, int y, int _z, float _scale) {
    z = _z;
    curPos = new PVector(x, y);
    animationType = a;
    imageCount = Animations.get(a).length;
    scale = _scale;
  }
  public int compareTo(Object o) {
    AnimEntity other = (AnimEntity) o;
    if (z != other.z) {
      if (z < other.z) return -1;
      return 1;
    }
    if (curPos.y < other.curPos.y) return -1;
    if (curPos.y > other.curPos.y) return 1;
    return 0;
  }
}

int decideDir(float goalDiffX) {
  if (goalDiffX > 0) return -1;
  if (goalDiffX < 0) return 1;
  return 1;
}

void drawAnim(PGraphics canvas, AnimEntity e) {
  canvas.imageMode(CENTER);
  canvas.pushMatrix();
  canvas.translate(e.curPos.x, e.curPos.y);
  canvas.scale(e.dir * e.scale, e.scale);
  canvas.image(Animations.get(e.animationType)[e.frame], 0, 0);
  canvas.popMatrix();
  e.frame = (e.frame+1) % e.imageCount;
}

final int FOLLOW_DIST = 80;
final int SEPARATION_RADIUS = 40;
final int BIRD_SPEED = 10;
void updateBirds(AnimEntity[] a) {
  AnimEntity leader = a[0];
  PVector[] p = {new PVector(0,100), new PVector(width, 100)};
  PVector goal = getPathGoal(leader, p);
  goal = wanderGoal(leader, goal);
  PVector leaderV = instantSeek(leader, goal, BIRD_SPEED);
  
  applyVelocity(leader, leaderV, goal, -1);
  PVector followV = leaderV.copy();
  followV.mult(-1);
  followV.normalize();
  followV.mult(FOLLOW_DIST);
  
  for(AnimEntity e : a) {
    if(e == leader)
      continue;
    PVector v = instantSeek(e, PVector.add(leader.curPos, followV), BIRD_SPEED);
    PVector f = new PVector(0, 0);
    for(AnimEntity n : a) {
      if(n != e && e.curPos.dist(n.curPos) < SEPARATION_RADIUS) {
        f.add(PVector.sub(n.curPos, e.curPos));
      }
    }
    f.mult(-1);
    f.normalize();
    f.mult(SEPARATION_RADIUS);
    v.add(f);
    applyVelocity(e, v, leader.curPos, -1);
  }
}

PVector instantSeek(AnimEntity e, PVector goal, float maxSpeed) {
  return PVector.sub(goal, e.curPos).limit(maxSpeed);
}

// returns velocity vector
PVector seek(AnimEntity e, PVector goal, float maxForce, float maxSpeed, float mass) {
  PVector desiredV = instantSeek(e, goal, maxSpeed);
  PVector steering = desiredV.sub(e.curV);
  steering.limit(maxForce);
  steering = steering.div(mass);

  PVector V = PVector.add(e.curV, steering).limit(maxSpeed);
  return V;
}

final int WAYPOINT_DIST = 50;
PVector getPathGoal(AnimEntity e, PVector[] path) {
  if(e.curPos.dist(path[e.curPathI]) < WAYPOINT_DIST) {
    e.curPathI = (e.curPathI + 1) % path.length;
  }
  return path[e.curPathI];
}

final float WANDER_DIST = 100;
final float WANDER_R = 100;
PVector wanderGoal(AnimEntity e, PVector goal) {
  PVector d = PVector.sub(goal, e.curPos);
  d.normalize();
  d.mult(WANDER_DIST);
  PVector c = PVector.add(e.curPos, d);
  c.add(new PVector(random(-WANDER_R, WANDER_R), random(-WANDER_R, WANDER_R)));
  //if(DEBUG)
  //  ellipse(c.x, c.y, 10, 10);
  return c;
}

void applyVelocity(AnimEntity e, PVector v, PVector goalP, int flip) {
  float distFromGoal = e.curPos.dist(goalP);
  e.curV = v;
  e.curPos.add(e.curV);
  if (distFromGoal > 10)
    e.dir = flip * decideDir(e.curPos.x - goalP.x);
}

void updateTiger(AnimEntity ti, PVector goal, PVector torch) {
  // run away from the torch
  if (PVector.dist(ti.curPos, goal) < TORCH_RADIUS + TORCH_BLUR_SIZE) {
    PVector v = instantSeek(ti, torch, 30);
    v.mult(-1); 
    applyVelocity(ti, v, torch, -1);
  } else {
    PVector v = instantSeek(ti, goal, 30); 
    applyVelocity(ti, v, pos, -1);
  }
}