#!/bin/sh

for i in `find . -maxdepth 1 -type d | grep -v '^\.$'`; do
    n=$(cd $i; ls -v | tail -n 1)
    t=`basename $n .png | sed -s 's/\-..$//g'`-00.png
    (cd $i; mv $n $t)
done
