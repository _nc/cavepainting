#!/bin/sh
[ -z "$1" ] && echo "need an argument" && exit 1
convert *.png -scale 25% $1%03d.png
